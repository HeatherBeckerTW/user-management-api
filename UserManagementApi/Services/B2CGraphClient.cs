﻿using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Microsoft.AspNetCore.Mvc;
using System.Web.Http;
using Newtonsoft.Json.Linq;

namespace UserManagementApi
{
    public class B2CGraphClient
    {
        #region //// Constants ////

        private const string AUTHENTICATION_HEADER_SCHEME_BEARER = "Bearer";
        private const string HTTP_METHOD_POST = "POST";
        private const string HTTP_METHOD_GET = "GET";
        private const string HTTP_METHOD_PUT = "PUT";
        private const string HTTP_METHOD_PATCH = "PATCH";
        private const string HTTP_METHOD_DELETE = "DELETE";
        private const string AAD_GRAPH_VERSION = "api-version=1.6";
        private const string MEDIA_TYPE_JSON = "application/json";
        private const string API_USERS = "users";
        private const string API_APPLICATIONS = "applications";
        private const string API_EXTENSIONS_PROPERTIES = "extensionProperties";

        #endregion

        #region //// Fields ////

        private bool _sendFullErrors = false;
        private string _graphInstance;
        private string _graphResource;
        private string _graphEndpoint;

        private string _tenant;
        private string _clientId;
        private string _clientSecret;

        private ClientCredential _credential;
        private AuthenticationContext _authContext;
        private static IConfiguration _configuration { get; set; }


        #endregion

        #region //// Constructors ////

        public B2CGraphClient(IConfiguration configuration)
        {
            _configuration = configuration;

            Init();
        }

        #endregion

        private void Init()
        {
            if(bool.TryParse(_configuration[$"SendErrors"], out bool testSend))
            {
                _sendFullErrors = testSend;
            }
            _graphInstance = _configuration[$"GraphApi:Instance"];
            _graphResource = _configuration[$"GraphApi:Resource"];
            _graphEndpoint = _configuration[$"GraphApi:Endpoint"];

            _tenant = _configuration[$"GraphApi:Tenant"];
            _clientId = _configuration[$"GraphApi:ClientId"];
            _clientSecret = _configuration[$"GraphApi:ClientSecret"];
        }

        private AuthenticationContext AuthContext()
        {
            if (_authContext == null)
            {
                _authContext = new AuthenticationContext($"{_graphInstance}/{_tenant}");
            }
            return _authContext;
        }

        private ClientCredential ClientCreds()
        {
            if (_credential == null)
            {
                _credential = new ClientCredential(_clientId, _clientSecret);
            }
            return _credential;
        }

        #region Public Methods

        public async Task<IActionResult> GetUserByObjectId(string objectId)
        {
            return await SendGraphGetRequestAsync($"{API_USERS}/{objectId}");
        }

        public async Task<IActionResult> GetAllUsersAsync(string query = "")
        {
            return await SendGraphGetRequestAsync(API_USERS, query);
        }

        public async Task<IActionResult> CreateUserAsync(string json)
        {
            return await SendGraphPostRequestAsync(API_USERS, json);
        }

        public async Task<IActionResult> UpdateUserAsync(string objectId, string json)
        {
            return await SendGraphPatchRequestAsync($"{API_USERS}/{objectId}", json);
        }

        public async Task<IActionResult> DeleteUserAsync(string objectId)
        {
            return await SendGraphDeleteRequestAsync($"{API_USERS}/{objectId}");
        }

        public async Task<IActionResult> RegisterExtensionAsync(string objectId, string body)
        {
            return await SendGraphPostRequestAsync($"{API_APPLICATIONS}/{objectId}/{API_EXTENSIONS_PROPERTIES}", body);
        }

        public async Task<IActionResult> UnregisterExtensionAsync(string appObjectId, string extensionObjectId)
        {
            return await SendGraphDeleteRequestAsync($"{API_APPLICATIONS}/{appObjectId}/{API_EXTENSIONS_PROPERTIES}/{extensionObjectId}");
        }

        public async Task<IActionResult> GetExtensionsAsync(string appObjectId)
        {
            return await SendGraphGetRequestAsync($"{API_APPLICATIONS}/{appObjectId}/{API_EXTENSIONS_PROPERTIES}");
        }

        public async Task<IActionResult> GetApplicationsAsync(string query)
        {
            return await SendGraphGetRequestAsync(API_APPLICATIONS, query);
        }

        #endregion

        private async Task<IActionResult> SendGraphDeleteRequestAsync(string api)
        {
            return await ContactGraphApi(HttpMethod.Delete, api);
        }

        private async Task<IActionResult> SendGraphPatchRequestAsync(string api, string json)
        {
            return await ContactGraphApi(new HttpMethod(HTTP_METHOD_PATCH), api, null, json);
        }

        private async Task<IActionResult> SendGraphPostRequestAsync(string api, string json)
        {
            return await ContactGraphApi(HttpMethod.Post, api, null, json);
        }

        public async Task<IActionResult> SendGraphGetRequestAsync(string api, string query = null)
        {
            return await ContactGraphApi(HttpMethod.Get, api, query);
        }

        private async Task<IActionResult> ContactGraphApi(HttpMethod method, string api, string query = null, string json = null)
        {
            // First, use ADAL to acquire a token using the app's identity (the credential)
            // The first parameter is the resource we want an access_token for; in this case, the Graph API.
            AuthenticationResult result = await this.AuthContext().AcquireTokenAsync(_graphResource, this.ClientCreds());

            // For B2C user managment, be sure to use the 1.6 Graph API version.
            HttpClient http = new HttpClient();
            string url = $"{_graphEndpoint}/{_tenant}/{api}?{AAD_GRAPH_VERSION}";
            if (!string.IsNullOrEmpty(query)) url = $"{url}&{query}";

            // Append the access token for the Graph API to the Authorization header of the request, using the Bearer scheme.
            HttpRequestMessage request = new HttpRequestMessage(method, url);
            request.Headers.Authorization = new AuthenticationHeaderValue(AUTHENTICATION_HEADER_SCHEME_BEARER, result.AccessToken);
            if (!string.IsNullOrEmpty(json)) request.Content = new StringContent(json, Encoding.UTF8, MEDIA_TYPE_JSON);
            HttpResponseMessage response = await http.SendAsync(request);

            if (!response.IsSuccessStatusCode)
            {
                string error = await response.Content.ReadAsStringAsync();
                //figure out what type of issue may have occurred and handle it appropriately
                if(error.Contains("signInNames already exists"))
                {
                    //username already exists - conflict (409)
                    return new ConflictObjectResult("The selected Sign In Name already exists.");
                }
                if(error.Contains("password complexity requirements"))
                {
                    //password isn't complex enough - 400 BADREQUEST
                    return new BadRequestObjectResult("The password provided does not meet complexity requirements.");
                }
                //else if (error.Contains("something else causing bad request - 400"))
                //{
                //    return new BadRequestObjectResult("Bad request - missing parameters.");
                //}
                //else if (error.Contains("business rules validation failure - 412"))
                //{
                //    return new BadRequestObjectResult("Bad request - business rules failure.");
                //}
                else
                {
                    return new ExceptionResult(new Exception(error), _sendFullErrors);
                }
            }

            string resultString = await response.Content.ReadAsStringAsync();
            IActionResult resultObject = null;
            switch (method.ToString())
            {
                case HTTP_METHOD_DELETE:
                    resultObject = new NoContentResult();
                    break;
                case HTTP_METHOD_GET:
                    resultObject = new OkObjectResult(resultString);
                    break;
                case HTTP_METHOD_PATCH:
                    resultObject = new OkResult();
                    break;
                case HTTP_METHOD_POST:
                    if (url.Contains(API_USERS))
                    {
                        JObject rss = JObject.Parse(resultString);
                        string userObjectId = (string)rss["objectId"];
                        string userLocation = $"{_graphEndpoint}/{_tenant}/{api}/{userObjectId}";
                        resultObject = new CreatedResult(userLocation, resultString);
                    }
                    else
                    {
                        resultObject = new CreatedResult("", resultString);
                    }
                    break;
                case HTTP_METHOD_PUT:
                    resultObject = new OkResult();
                    break;
                default:
                    resultObject = new OkObjectResult(resultString);
                    break;
            }

            return resultObject;
        }
    }
}
