﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using UserManagementApi.Models;


namespace UserManagementApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class UsersController : Controller
    {
        private IConfiguration _configuration;
        B2CGraphClient _graph;

        public UsersController(IConfiguration configuration)
        {
            _configuration = configuration;
            _graph = new B2CGraphClient(_configuration);
        }

        // GET api/values
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                if (!HasNecessaryPermissions(Startup.ScopeRead))
                    return Unauthorized();

                return await _graph.GetAllUsersAsync();
            }
            catch(Exception ex)
            {
                return new ExceptionResult(new Exception("An unexpected error occurred and processing could not continue."), false);
            }   
        }
        
        // GET api/values/454bd9a9-7a7c-4e7f-9535-213e07408d14
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(string id)
        {
            try
            {
                if (!HasNecessaryPermissions(Startup.ScopeRead))
                    return Unauthorized();

                return await _graph.GetUserByObjectId(id);
            }
            catch (Exception ex)
            {
                return new ExceptionResult(new Exception("An unexpected error occurred and processing could not continue."), false);
            }
        }

        // POST (Create) api/values
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] User value)
        {
            try
            {
                if (!HasNecessaryPermissions(Startup.ScopeCreate))
                    return Unauthorized();

                string json = value.ToString();
                return await _graph.CreateUserAsync(value.ToString());
            }
            catch (Exception ex)
            {
                return new ExceptionResult(new Exception("An unexpected error occurred and processing could not continue."), false);
            }
}

        // PUT (Update) api/values/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(string id, [FromBody] User value)
        {
            try
            {
                //update an existing user
                if (!HasNecessaryPermissions(Startup.ScopeUpdate))
                    return Unauthorized();

                //Currently the only update to a user that's performed would be to update their password
                //TODO: Are there other things that might need to be updated?
                string json = value.passwordProfile.ToJson();

                return await _graph.UpdateUserAsync(id, value.passwordProfile.ToJson());
            }
            catch (Exception ex)
            {
                return new ExceptionResult(new Exception("An unexpected error occurred and processing could not continue."), false);
            }
}

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(string id)
        {
            try
            {
                //delete an existing user
                if (!HasNecessaryPermissions(Startup.ScopeDelete))
                    return Unauthorized();

                return await _graph.DeleteUserAsync(id);
            }
            catch (Exception ex)
            {
                return new ExceptionResult(new Exception("An unexpected error occurred and processing could not continue."), false);
            }
        }

        private bool HasNecessaryPermissions(string requiredScopeForAccess)
        {
            //return true;

            var userScopes = HttpContext.User.FindFirst(_configuration[$"Authentication:B2C:ScopeSchema"])?.Value;

            return (!string.IsNullOrEmpty(requiredScopeForAccess) 
                && !string.IsNullOrEmpty(userScopes) 
                && userScopes.Split(' ').Any(s => s.Equals(requiredScopeForAccess)));
        }
    }
}
