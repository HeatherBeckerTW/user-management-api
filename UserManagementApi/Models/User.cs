﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UserManagementApi.Models
{
    [Serializable]
    public class User
    {
        public bool accountEnabled { get; set; }
        public string creationType { get; set; }
        public string displayName { get; set; }
        public PasswordProfile passwordProfile { get; set; }
        public SignInNames[] signInNames { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this, Formatting.None);
        }
    }

    [Serializable]
    public class PasswordProfile
    {
        public string password { get; set; }
        public bool forceChangePasswordNextLogin { get; set; }

        public string ToJson(bool includeParentObject = true)
        {
            if (includeParentObject)
            {
                string forcePWReset = forceChangePasswordNextLogin.ToString().ToLower();
                return $"{{\"passwordProfile\": {{\"password\": \"{password}\",\"forceChangePasswordNextLogin\": \"{forcePWReset}\"}}}}";
            }
            else
                return JsonConvert.SerializeObject(this, Formatting.None);
        }
    }

    [Serializable]
    public class SignInNames
    {
        public string type { get; set; }
        public string value { get; set; }
    }
}
